import Login from "./Login";
import Dashboard from "./Dashboard"
import React from 'react';

function App() {
    function checkLogin(){
        return localStorage.getItem('token');
    }

    return checkLogin() ? <Dashboard/> : <Login/>
}

export default App;
