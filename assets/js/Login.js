import React, {useState} from 'react';
import { useNavigate } from "react-router-dom";

function Login() {

    const [error, setError] = useState('')
    function handleLogin(e){
        e.preventDefault();



        fetch('/api/login-check', {
            credentials: 'same-origin',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: e.target.loginemail.value,
                password : e.target.loginpassword.value
            }),
        })
            .then(response => {
                if (response.status === 401) {
                    setError('Wrong email or password.')
                } else {
                    response.json().then((data) => {
                        localStorage.setItem('token', data.token);
                        localStorage.setItem('refresh_token', data.refresh_token);
                        window.location.reload();
                    });
                }
            });
    }


    return(
        <>
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-lg-4">
                        <div className="card p-2">
                            <h1 className="mb-3">Log in</h1>
                            {error && <h1>{error}</h1>}
                            <form onSubmit={handleLogin}>
                                <div className="mb-2">
                                    <label htmlFor="loginemail" className="form-label">Email</label>
                                    <input type="email" className="form-control" id="loginemail" placeholder="someemail@address.com" />
                                    <div className="feedback invalid-feedback"></div>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="loginpassword" className="form-label">Password</label>
                                    <input type="password" className="form-control" id="loginpassword" placeholder="Shhh its a secret" />
                                    <div className="feedback invalid-feedback"></div>
                                </div>
                                <button className="btn btn-primary w-100 mb-2" type="submit">Sign in</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Login;
