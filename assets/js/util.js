
export function makeRequest(url, callback) {
    fetch(url, getHeaders()).then(response => {
        if (response.status === 401) {
            fetch('/token/refresh', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    'refresh_token': localStorage.getItem('refresh_token')
                }),
            }).then((response) => {
                response.json().then((data) => {
                    localStorage.setItem('token', data.token);
                    localStorage.setItem('refresh_token', data.refresh_token);
                    window.location.reload();
                })
            })
        } else {
            response.json().then(callback)
        }
    })

}

export function getHeaders() {
    return {
        credentials: 'same-origin',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    }
}

export function getTemperatureSymbol(unit) {
    return unit === 'm' ? '°C' : 'F';
}

export function getSpeedSymbol(unit) {
    return unit === 'm' ? 'm/s' : 'f/s'
}
