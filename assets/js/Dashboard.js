import React, {useEffect} from 'react';
import {useState} from 'react';
import {getHeaders, getSpeedSymbol, makeRequest} from "./util";
import {getTemperatureSymbol} from "./util";

function Dashboard() {
    const [result, setResult] = useState('')
    const [locations, setLocations] = useState([])

    function loadLocations() {
        makeRequest('/api/locations', (data) => {
            setLocations(data)
        })
    }

    useEffect(() => {
        loadLocations()
    }, [])

    function handleAverage(id) {
        makeRequest(`/api/${id}/average`, (data) => {
            setResult(`Average temperature in ${data.name} is ${data.average} ${getTemperatureSymbol(data.unit)}`)
        })
    }

    function handleToday(id) {
        makeRequest(`/api/${id}/today`, (data) => {
            setResult(`Today is ${data.temperature} ${getTemperatureSymbol(data.unit)} and ${data.wind} ${getSpeedSymbol(data.unit)}`)
        })
    }

    return (
        <div>
            <div>
                {locations.map((location) => (
                    <div>
                        <button
                            key={'average' + location.id}
                            onClick={() => handleAverage(location.id)}
                        >
                            {location.name} last 7 days average
                        </button>
                        <button
                            key={'today' + location.id}
                            onClick={() => handleToday(location.id)}
                        >
                            {location.name} weather today
                        </button>
                    </div>
                ))}
            </div>
            <div>{result}</div>
        </div>


    )
}

export default Dashboard
