<?php
declare(strict_types=1);

namespace App\Factory;

use App\Entity\Location;
use App\Entity\Weather;

final class WeatherFactory
{
    public function createWeatherStack(Location $location, array $payload): Weather
    {
        $weather = new Weather();
        $weather->setLocation($location);
        $weather->setPayload($payload);
        $weather->setCreatedAt(new \DateTimeImmutable());
        $weather->setTemperature($payload['current']['temperature']);
        $weather->setWind($payload['current']['wind_speed']);

        return $weather;
    }
}
