<?php
declare(strict_types=1);

namespace App\Factory;

use App\Entity\Location;

final class LocationFactory
{
    public function createWeatherStack(string $rawLocation, string $units): Location
    {
        $location = new Location();
        $location->setName($rawLocation);
        $location->setUnits($units);
        $location->setProvider(Location::PROVIDER_WEATHERSTACK);

        return $location;
    }
}
