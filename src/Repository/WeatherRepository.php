<?php

namespace App\Repository;

use App\Entity\Location;
use App\Entity\Weather;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Weather>
 *
 * @method Weather|null find($id, $lockMode = null, $lockVersion = null)
 * @method Weather|null findOneBy(array $criteria, array $orderBy = null)
 * @method Weather[]    findAll()
 * @method Weather[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeatherRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Weather::class);
    }

    public function findOneByLocationToday(Location $location): ?Weather
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.location = :location')
            ->andWhere('w.createdAt BETWEEN :from AND :to')
            ->setParameter('location', $location)
            ->setParameter('from', (new \DateTime())->setTime(0,0))
            ->setParameter('to', (new \DateTime())->setTime(23, 59))
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @return Weather[]
     */
    public function findTemperatureSevenDaysByLocation(Location $location): array
    {
        $weekago = (new \DateTime())->modify('-1 week');

        return $this->createQueryBuilder('w')
            ->select('w.temperature')
            ->andWhere('w.location = :location')
            ->andWhere('w.createdAt > :weekago')
            ->setParameters([
                'location' => $location,
                'weekago' => $weekago,
            ])
            ->getQuery()
            ->getSingleColumnResult()
        ;
    }
}
