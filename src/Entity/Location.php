<?php

namespace App\Entity;

use App\Repository\LocationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LocationRepository::class)]
class Location
{
    public const PROVIDER_WEATHERSTACK = 1;
    public const UNIT_METRIC = 'm';
    public const UNIT_FAHRENHEIT = 'f';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 1)]
    private ?string $units = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $provider = null;

    #[ORM\OneToMany(mappedBy: 'location', targetEntity: Weather::class, orphanRemoval: true)]
    private Collection $weathers;

    public function __construct()
    {
        $this->weathers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getUnits(): ?string
    {
        return $this->units;
    }

    public function setUnits(string $units): static
    {
        $this->units = $units;

        return $this;
    }

    public function getProvider(): ?int
    {
        return $this->provider;
    }

    public function setProvider(int $provider): static
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * @return Collection<int, Weather>
     */
    public function getWeathers(): Collection
    {
        return $this->weathers;
    }

    public function addWeather(Weather $weather): static
    {
        if (!$this->weathers->contains($weather)) {
            $this->weathers->add($weather);
            $weather->setLocation($this);
        }

        return $this;
    }

    public function removeWeather(Weather $weather): static
    {
        if ($this->weathers->removeElement($weather)) {
            // set the owning side to null (unless already changed)
            if ($weather->getLocation() === $this) {
                $weather->setLocation(null);
            }
        }

        return $this;
    }
}
