<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Location;
use App\Repository\LocationRepository;
use App\Repository\WeatherRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

final class WeatherController extends AbstractController
{
    private WeatherRepository $weatherRepository;
    private LocationRepository $locationRepository;

    public function __construct(WeatherRepository $weatherRepository, LocationRepository $locationRepository)
    {
        $this->weatherRepository = $weatherRepository;
        $this->locationRepository = $locationRepository;
    }

    #[Route('/api/locations', name: 'weather_location_list')]
    public function getLocations(): JsonResponse
    {
        return new JsonResponse(array_map(function(Location $location) {
            return [
                'id' => $location->getId(),
                'name' => $location->getName(),
            ];
        }, $this->locationRepository->findAll()));
    }

    #[Route('/api/{locationId}/average', name: 'weather_location_average', requirements: ['locationId' => '\d+'])]
    public function getWeather7daysAverage(int $locationId): JsonResponse
    {
        $location = $this->locationRepository->find($locationId);
        if ($location === null) {
            throw new NotFoundHttpException();
        }

        $temperatures = $this->weatherRepository->findTemperatureSevenDaysByLocation($location);
        if ($temperatureCount = count($temperatures)) {
            $average = array_sum($temperatures) / $temperatureCount;
        } else {
            $average = 0;
        }


        return new JsonResponse([
            'average' => $average,
            'unit' => $location->getUnits(),
            'name' => $location->getName(),
        ]);
    }

    #[Route('/api/{locationId}/today', name: 'weather_location_today', requirements: ['locationId' => '\d+'])]
    public function getLocationToday(int $locationId): JsonResponse
    {
        $location = $this->locationRepository->find($locationId);
        if ($location === null) {
            throw new NotFoundHttpException();
        }

        $weather = $this->weatherRepository->findOneByLocationToday($location);

        return new JsonResponse([
            'name' => $location->getName(),
            'unit' => $location->getUnits(),
            'wind' => $weather->getWind(),
            'temperature' => $weather->getTemperature(),
        ]);
    }
}
