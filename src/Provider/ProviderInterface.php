<?php
declare(strict_types=1);

namespace App\Provider;

use App\Entity\Location;

interface ProviderInterface
{
    public function getData(Location $location): array;
}
