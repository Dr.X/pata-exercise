<?php
declare(strict_types=1);

namespace App\Provider;

use App\Entity\Location;
use App\Exception\LocationNotFoundException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class WeatherStackProvider implements ProviderInterface
{
    private HttpClientInterface $httpClient;
    private string $weatherStackApiKey;
    private RouterInterface $router;

    public function __construct(
        HttpClientInterface $httpClient,
        RouterInterface $router,
        #[\SensitiveParameter] string $weatherStackApiKey
    ) {
        $this->httpClient = $httpClient;
        $this->weatherStackApiKey = $weatherStackApiKey;
        $this->router = $router;
    }

    /**
     * @throws LocationNotFoundException
     */
    public function getData(Location $location): array
    {
        $route = $this->router->generate('weatherstack_current', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $url = $route . '?' . http_build_query([
                'query' => $location->getName(),
                'access_key' => $this->weatherStackApiKey,
                'units' => $location->getUnits(),
            ]);

        $response = $this->httpClient->request('GET', $url);
        $content = $response->getContent();
        $result = json_decode($content, true);
        if (array_key_exists('success', $result) && $result['success'] === false) {
            throw new LocationNotFoundException();
        }

        return $result;
    }
}
