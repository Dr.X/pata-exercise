<?php
declare(strict_types=1);

namespace App\Command;

use App\Entity\Location;
use App\Manager\WeatherManagerInterface;
use App\Provider\ProviderInterface;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:manage-weather',
)]
final class ManageWeatherCommand extends Command {
    private WeatherManagerInterface $weatherManager;

    public function __construct(WeatherManagerInterface $weatherManager, EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->weatherManager = $weatherManager;
    }

    protected function configure()
    {
        $this
            ->addOption(
                'location',
                'l',
                InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY,
                'Name of location, for example "-l \"New York\"'
            )
            ->addOption(
                'unit',
                'u',
                InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY,
                'unit measurement ("m" or "f" only)'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $rawLocations = $input->getOption('location');
        $units = $input->getOption('unit');

        $locations = $this->validateInputData($rawLocations, $units);
        $this->weatherManager->processLocations($locations);
        $io->success('Weather for locations are saved!');

        return Command::SUCCESS;
    }

    private function validateInputData(array $rawLocations, array $units): array
    {
        if (count($rawLocations) !== count($units)) {
            throw new InvalidArgumentException('Locations and units counts are not identical.');
        }
        $locations = [];
        foreach ($rawLocations as $key => $location) {
            $unit = $units[$key];
            if (!in_array($unit, [Location::UNIT_FAHRENHEIT, Location::UNIT_METRIC])) {
                throw new InvalidArgumentException('Wrong measurement unit in argument');
            }
            $locations[$location] = $unit;
        }

        return $locations;
    }
}
