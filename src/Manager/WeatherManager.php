<?php
declare(strict_types=1);

namespace App\Manager;

use App\Exception\LocationNotFoundException;
use App\Factory\LocationFactory;
use App\Factory\WeatherFactory;
use App\Provider\ProviderInterface;
use App\Repository\LocationRepository;
use App\Repository\WeatherRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

final class WeatherManager implements WeatherManagerInterface
{
    private ProviderInterface $provider;
    private LocationRepository $locationRepository;
    private LocationFactory $locationFactory;
    private EntityManagerInterface $entityManager;
    private WeatherFactory $weatherFactory;
    private WeatherRepository $weatherRepository;
    private LoggerInterface $logger;

    public function __construct(
        ProviderInterface      $provider,
        WeatherFactory         $weatherFactory,
        LocationRepository     $locationRepository,
        LocationFactory        $locationFactory,
        EntityManagerInterface $entityManager,
        WeatherRepository      $weatherRepository,
        LoggerInterface $logger,
    )
    {
        $this->provider = $provider;
        $this->locationRepository = $locationRepository;
        $this->locationFactory = $locationFactory;
        $this->entityManager = $entityManager;
        $this->weatherFactory = $weatherFactory;
        $this->weatherRepository = $weatherRepository;
        $this->logger = $logger;
    }

    /**
     * @param array{int: array{string, string}} $locations
     */
    public function processLocations(array $locations): void
    {
        foreach ($locations as $rawLocation => $unit) {
            if ($location = $this->locationRepository->findOneByLocationName($rawLocation)) {
                $location->setUnits($unit);
                $isLocationExisted = true;
            } else {
                $location = $this->locationFactory->createWeatherStack($rawLocation, $unit);
                $this->entityManager->persist($location);
                $isLocationExisted = false;
            }
            if ($isLocationExisted && $this->weatherRepository->findOneByLocationToday($location)) {
                $this->logger->info('Weather for today already saved in DB, skipping');
            } else {
                try {
                    $rawWeather = $this->provider->getData($location);
                } catch (LocationNotFoundException $e) {
                    $this->logger->error(sprintf('Location %s is not found, skipping', $location->getName()));
                    continue;
                }
                $weather = $this->weatherFactory->createWeatherStack($location, $rawWeather);
                $this->entityManager->persist($weather);
            }
        }

        $this->entityManager->flush();
    }
}
