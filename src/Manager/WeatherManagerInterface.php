<?php
declare(strict_types=1);

namespace App\Manager;

interface WeatherManagerInterface
{
    /**
     * @param array{int: array{string, string}} $locations
     */
    public function processLocations(array $locations): void;
}
