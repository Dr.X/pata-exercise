### requirements
* node v20
* yarn
* `apt-get install php8.2-sqlite`
* Symfony Installer should be installed (to run dev server)

### install
* `composer install`
* `yarn install`
* `php bin/console lexik:jwt:generate-keypair`
* copy file `app.db.example` to `app.db`
* `yarn run dev`
* `symfony server:start`

### Usage
1. populate database with weather with command `php bin/console app:manage-weather -l "new york" -u m -l "riga" -u f`
2. run server with `symfony server:start`
3. go to http://localhost:8000 and enter login `admin@admin.lv` and password `password`
